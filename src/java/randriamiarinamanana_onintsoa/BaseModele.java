/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

/**
 *
 * @author ITU
 */
public abstract class BaseModele {
    protected String[] title;
    protected String id;
    
    public String[] getTitle() {
        return title;
    }

    public void setTitle(String[] title) {
        this.title = title;
    }
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    
    public abstract void save();
            
    public abstract void update(Object object);
    
    public abstract Object[] find ();
}
